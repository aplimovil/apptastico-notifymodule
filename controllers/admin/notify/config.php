<?php
$this->loadModuleController('local/System', 'admin/module-config');

class LocalNotify_AdminNotifyConfigController extends LocalSystem_AdminModuleConfigController {
	function __construct(){
		global $USER, $YES_NO;

		if($USER->type != USER_TYPE_ADMIN){
			$this->quit(_T('Lo sentimos, no tiene permiso para acceder a esta sección.'));
		}

		self::$module = Notify\Config::$moduleName;

		self::$formWidth = '100%';

		self::$title = _T('Configuración de Notificaciones');

		self::$fields = [
			'sender_name' => [
				'heading' => _T('Opciones Generales'),
				'label' => _T('Nombre de Remitente'),
				'required' => true,
				'default' => Notify\Config::$defaultOptions['sender_name'],
				'comments' => _T('Indique el remitente (nombre de quien envía) con el que desea que aparezcan las notificaciones para los usuarios. Solo aplica para dispositivos Android. En dispositivos iOS el remitente es el nombre de la aplicación.')
			],
			'android_sender_id' => [
				'label' => _T('Android Sender ID'),
				'required' => true,
				'comments' => _T('El Android Sender ID se obtiene de la sección de "Cloud Messaging" dentro de "Project Settings" de un proyecto en el <a href="https://console.firebase.google.com/" target="_blank">Google Firebase Console</a>.')
			],
			'aws_credentials_key' => [
				'heading' => _T('Opciones AWS'),
				'label' => _T('Access Key ID'),
				'required' => true
			],
			'aws_credentials_secret' => [
				'label' => _T('Secret Access Key'),
				'size' => 60,
				'required' => true
			],
			'aws_region' => [
				'label' => _T('Region ID'),
				'required' => true,
				'default' => Notify\Config::$defaultOptions['aws_region']
			],
			'aws_sns_application_arn_ios' => [
				'heading' => _T('Opciones SNS'),
				'label' => _T('Application ARN para iOS'),
				'required' => true,
				'size' => 80
			],
			'aws_sns_application_arn_android' => [
				'label' => _T('Application ARN para Android'),
				'required' => true,
				'size' => 80
			],
			'aws_sns_topic_arn_list' => [
				'label' => _T('Topic ARN'),
				'required' => true,
				'size' => 80
			]
			/*
			'aws_sns_topic_arn_list' => [
				'label' => _T('Topic ARN\'s'),
				'type' => 'textarea',
				'required' => true,
				'comments' => _T('Lista de Topic ARN\'s entre los cuales desea distribuir las suscripciones. Cada ARN debe estar separado por un salto de línea.')
			]
			*/
		];
	}
}