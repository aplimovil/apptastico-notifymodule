<?php
class LocalNotify_AdminNotifyNotificationConfirmationsController extends CatalogController {
	function main($APP){
		$notification = notification::get($_GET['notificationId']);

		$this->init(_T('Notificación "%s" &raquo; Reporte de Confirmaciones', $notification->subject), 'notification_confirmation', _T('confirmación'))
		->configure([
			'reportMode' => true,
			'allowSearch' => false,
			'width' => 700
		])
		->setParent($notification)
		->orderBy('datetime', 'DESC')
		->addFields([
			'person_entity_id' => [
				'label' => _T('Persona'),
				'searchable' => false,
				'format' => 'function',
				'function' => function($personEntityId, $confirmation){
					if($model = $confirmation->person_model){
						if(Util::classHasTrait($model, 'Entity')){
							$entity = $model::get($personEntityId);
							return sprintf('<a href="%s">%s</a>', $entity->getCatalogLink(), $entity->getName());
						}
						else{
							return sprintf('%s #%s', $model, $personEntityId);
						}
					}
					else return '(' . _T('Anónimo') . ')';
				}
			],
			'datetime' => [
				'label' => _T('Fecha/Hora'),
				'type' => 'date',
				'dateFormat' => 'd/m/Y g:i a'
			]
		]);
	}
}