<?php
use Notify\Config as Config;

class LocalNotify_AdminNotifyNotificationsController extends CatalogController {
	static $formFields = ['subject', 'status', 'datetime_sent', 'recipient_count', 'image', 'content'];
	static $enableEditForm = true;

	protected $totalRecipients;

	function main($APP){
		$allowNotifications = $this->checkNotificationsAvailable();

		$this->totalRecipients = Notify\System::getSubscriberCount();

		if($_GET['cmd'] == 'send-notification'){
			if($APP->validateGlobalToken($_GET['token'])){
				$this->sendNotification();
			}
		}

		$APP->generateGlobalToken();

		$catalog = $this->init(_T('Notificaciones'), 'notification', _T('notificación'))
		->configure([
			'width' => '100%',
			'dialogWidth' => 650,
			'dialogHeight' => (Config::$useConfirmations ? 200 : 120),
			'sideLinksWidth' => 420,
			'langAltMode' => true
		])
		->setOrder('id', _T('ID'), 'DESC')
		->addFields([
			'subject' => [
				'label' => _T('Asunto'),
				'required' => true,
				'size' => 50,
				'form' => [
					'type' => 'readonly'
				]
			],
			'status' => [
				'label' => _T('Status'),
				'catalog' => Notify\Config::$NOTIFICATION_STATUS,
				'editable' => false,
				'form' => [
					'type' => 'readonly'
				]
			],
			'datetime_sent' => [
				'label' => _T('Fecha/Hora de Envío'),
				'type' => 'date',
				'size' => 12,
				'editable' => false,
				'sortable' => true,
				'format' => 'function',
				'function' => function($datetime){
					$timestamp = strtotime($datetime);

					if($datetime && $timestamp > 1000){
						return date('d/m/Y g:i a', $timestamp);
					}
					else{
						return '<em>' . _T('Envío pendiente') . '</em>';
					}
				},
				'form' => [
					'type' => 'function'
				]
			],
			'recipient_count' => [
				'label' => _T('# Destinatarios'),
				'editable' => false,
				'format' => 'number',
				'form' => [
					'type' => 'function',
					'function' => function($count, $notification){
						return _T('%s destinatarios', $count ?: '0');
					}
				]
			],
			'read_count' => [
				'label' => _T('# Aperturas'),
				'editable' => false,
				'size' => 5,
				'rangeSearch' => true,
				'format' => 'function',
				'sortable' => true,
				'condition' => Config::$useReadTracking,
				'function' => function($count, $notification){
					if($notification->status == Notify\NOTIFICATION_STATUS_SENT){
						if($count > $notification->recipient_count) $count = $notification->recipient_count;
						$percentage = $notification->recipient_count != 0 ? round(($count / $notification->recipient_count) * 100) : 0;
						return sprintf('%d (%d%%)', $count, $percentage);
					}
				}
			],
			'image' => [
				'label' => _T('Imagen Adjunta (opcional)'),
				'listable' => false,
				'editable' => false,
				'searchable' => false,
				'form' => [
					'type' => 'upload',
					'extensions' => 'jpg,jpeg,gif,png',
					'folder' => Notify\Config::$UPLOAD_FOLDER_IMAGES,
					'allowView' => true,
					'comments' => _T('Tipos de archivo permitidos: JPEG, GIF y PNG')
				]
			],
			'content' => [
				'label' => _T('Contenido'),
				'listable' => false,
				'editable' => false,
				'searchable' => false,
				'form' => [
					'required' => true,
					'type' => 'htmleditor'
				]
			],
			'request_confirmations' => [
				'label' => _T('Solicitar Confirmaciones'),
				'type' => 'checkbox',
				'condition' => Config::$useConfirmations
			],
			'confirmation_button_label' => [
				'label' => _T('Etiqueta de Botón de Confirmación'),
				'required' => true,
				'listable' => false,
				'searchable' => false,
				'default' => _T('Enviar Confirmación'),
				'depends' => [
					'field' => 'request_confirmations',
					'condition' => function($requestConfirmations){
						return ($requestConfirmations == 1);
					}
				],
				'condition' => Config::$useConfirmations
			],
			'confirmation_count' => [
				'label' => _T('# Confirmaciones'),
				'editable' => false,
				'rangeSearch' => true,
				'format' => 'function',
				'sortable' => true,
				'function' => function($count, $notification){
					if($notification->request_confirmations){
						if($notification->status == Notify\NOTIFICATION_STATUS_SENT){
							$percentage = $notification->recipient_count != 0 ? round(($count / $notification->recipient_count) * 100) : 0;
							return sprintf('%d (%d%%)', $count, $percentage);
						}
					}
					else return '<em>' . _T('No Aplica') . '</em>';
				},
				'condition' => Config::$useConfirmations
			]
		]);

		if($allowNotifications){
			$catalog->addLinksBefore(
				[
					'label' => 'Enviar Notificación',
					'href' => '#{id}',
					'fields' => ['id'],
					'attributes' => [
						'onclick' => 'Notify.Notifications.sendNotification(this.href); return false;'
					],
					'condition' => function($notification){
						return ($notification->status == Notify\NOTIFICATION_STATUS_PENDING);
					}
				],
				[
					'label' => 'Reenviar Notificación',
					'href' => '#{id}',
					'fields' => ['id'],
					'attributes' => [
						'onclick' => 'Notify.Notifications.sendNotification(this.href); return false;'
					],
					'condition' => function($notification){
						return ($notification->status == Notify\NOTIFICATION_STATUS_SENT);
					}
				]
			);
		}
		else{
			$catalog->configure([
				'allowCreate' => false
			]);
		}

		if(static::$enableEditForm){
			$catalog->enableEditForm(_T('Editar Contenido'), static::$formFields);
		}

		if(Config::$useReadTracking && Config::$useReadReport){
			$catalog->addLinksAfter(
				[
					'label' => _T('Consultar Aperturas'),
					'path' => 'admin/notify/notification/{id}/reads',
					'condition' => function($notification){
						return ($notification->status == Notify\NOTIFICATION_STATUS_SENT);
					}
				]
			);
		}

		if(Config::$useConfirmations){
			$catalog->addLinksAfter(
				[
					'label' => _T('Consultar Confirmaciones'),
					'path' => 'admin/notify/notification/{id}/confirmations',
					'condition' => function($notification){
						return ($notification->request_confirmations == 1 && $notification->status == Notify\NOTIFICATION_STATUS_SENT);
					}
				]
			);
		}
	}

	function checkNotificationsAvailable(){
		if(!Config::$allowCreateNotifications) return false;

		try{
			$SNS = Notify\System::getSNSInstance();
		}
		catch(Exception $e){
			$this->error(_T('ALERTA: %s', $e->getMessage()));
			return false;
		}

		$monthlyLimit = Notify\Config::$LIMIT_MONTHLY_NOTIFICATIONS;
		$notificationsSentThisMonth = Notify\System::getNotificationsSentThisMonth();

		if($notificationsSentThisMonth >= $monthlyLimit){
			$this->error(_T('Ha excedido su límite de %s mensajes de notificación mensuales. Por favor contacte al administrador del sistema si desea elevar este límite.', number_format($monthlyLimit)));
			return false;
		}
		else{
			$monthlyUsage = ($notificationsSentThisMonth / $monthlyLimit) * 100;

			if($monthlyUsage >= 99){ // Avisar cuando el limite de uso este al 99%
				$remainingNotificationsThisMonth = Notify\System::getRemainingNotificationsThisMonth();
				$this->message("AVISO: Actualmente ha usado el " . number_format($monthlyUsage, 2) . "% de su limite de uso, y le quedan " . number_format($remainingNotificationsThisMonth) . " mensajes de notificación disponibles para este mes.");
			}
		}

		return true;
	}

	public static function getSNSInstance(){
		global $APP;

		$APP->loadLibrary('AWS');
		
		$key = Config::getOption('aws_credentials_key');
		$secret = Config::getOption('aws_credentials_secret');
		$region = Config::getOption('aws_region');

		if($key == '') throw new Exception(_T('Aun no se ha configurado el Access Key ID de Amazon Web Services.'));
		if($secret == '') throw new Exception(_T('Aun no se ha configurado el Access Secret Key de Amazon Web Services.'));
		if($region == '') throw new Exception(_T('Aun no se ha configurado el region ID de Amazon Web Services.'));

		$SNS = Aws\Sns\SnsClient::factory([
		    'key' => $key,
		    'secret' => $secret,
		    'region'  => $region
		]);

		return $SNS;
	}

	function sendNotification(){
		$notificationId = $_GET['id'];

		if(is_numeric($notificationId)){
			if($notification = notification::get($notificationId)){
				$remainingNotificationsThisMonth = Notify\System::getRemainingNotificationsThisMonth();

				if($remainingNotificationsThisMonth >= $notification->recipient_count){
					try{
						$notification->send();

						$this->message(_T('La notificación fue enviada exitosamente a %s usuarios.', number_format($notification->recipient_count)));
					}
					catch(Exception $e){
						$this->error($e->getMessage());
					}
				}
				else{
					$this->error(_T('Lo sentimos, el envío de esta notificación a %s suscriptores excede los %s mensajes de notificación que quedan disponibles en este mes. Por favor contacte al administrador del sistema si desea elevar su límite de envío mensual.', number_format($notification->recipient_count), number_format($remainingNotificationsThisMonth)));
				}
			}
		}
	}
}