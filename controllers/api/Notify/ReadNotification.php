<?php
class LocalNotify_ApiNotifyReadNotificationController extends APIController {
	function main($APP, $GET, $POST){
		$this->authorize();
		
		if(!is_numeric($POST->notification_id)){
			$this->error(_T('Parámetros requeridos no proporcionados.'));
		}

		$notification = notification::get($POST->notification_id);
		if(!$notification) $this->error(_T('Notificación no encontrada.'));

		DB::Begin();

		$params = [
			'notification_id' => $notification->id,
			'ip_address' => Util::getRealIPAddress()
		];

		if($POST->person_model){
			Util::extend($params, [
				'person_model' => $POST->person_model,
				'person_entity_id' => $POST->person_entity_id
			]);
		}

		if(DB::Count('notification_read', $params) == 0){
			$notificationRead = (new notification_read($params))->set([
				'datetime' => date('Y-m-d H:i:s')
			]);

			$notificationRead->save();

			$notification->read_count ++;
			$notification->save();

			DB::Commit();

			$this->send();
		}
		else{
			$this->send(['message' => 'Lectura ya registrada.']);
		}
	}
}