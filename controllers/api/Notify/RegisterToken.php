<?php
class LocalNotify_ApiNotifyRegisterTokenController extends APIController {
	static $debugMode = false;

	function main($APP, $GET, $POST){
		$token = trim($_REQUEST['token']);
		$oldToken = trim($_REQUEST['old_token']);
		$platform = strtolower(trim($_REQUEST['platform']));
		$registerInSNS = true;

		if(session_status() == PHP_SESSION_ACTIVE){
			session_write_close();
		}

		if($token != ''){
			$SNS = Notify\System::getSNSInstance();

			/* Obtener el Amazon SNS Application ARN */
			if($platform == 'ios'){
				$applicationARN = Notify\Config::getOption('aws_sns_application_arn_ios');
			}
			elseif($platform == 'android'){
				$applicationARN = Notify\Config::getOption('aws_sns_application_arn_android');
			}
			else{
				$registerInSNS = false;
				$endpointARN = '';
				$subscriptionARN = '';
			}

			if($registerInSNS){
				$applicationARN = trim($applicationARN);

				if(!$applicationARN) $this->error(_T("No hay ningún Application ARN disponible."));

				$endpointARN = '';
				$subscriptionARN = '';

				/* Obtener el SNS Topic */
				$topicARN = trim(Notify\Config::getOption('aws_sns_topic_arn_list'));

				if(!$topicARN){
					$this->quit(_T('Aun no esta configurado el Topic ARN.'));
				}

				/*
				try{
					$topic = Notify\System::getTopicFromList();
					$topicARN = $topic->arn;
				}
				catch(Exception $e){
					$this->error(_T("No se encontró ningún Topic ARN disponible para realizar la suscripción (Error: %s).", $e->getMessage()));
				}
				*/

				if($oldToken){
					if($oldToken != $token){
						if($oldPushToken = pushtoken::findOne(['token' => $oldToken])){
							try{
								$SNS->unsubscribe([
									'SubscriptionArn' => $oldPushToken->amazonsns_subscription_arn
								]);

								$SNS->deleteEndpoint([
									'EndpointArn' => $oldPushToken->amazonsns_endpoint_arn
								]);
							}
							catch(Exception $e){
								$this->error(_T('Ocurrió un error al actualizar el push token anterior: %s', $e->getMessage()));
							}

							$oldPushToken->delete();
						}
					}
					else{
						if($pushtoken = pushtoken::findOne(['token' => $token])){
							// Forzar que el endpoint este habilitado en caso de que se haya deshabilitado previamente (ej. al desinstalar el app o desactivar las noticiaciones a nivel del OS)
							$SNS->setEndpointAttributes(array(
								'EndpointArn' => $pushtoken->amazonsns_endpoint_arn,
								'Attributes' => array(
									'Enabled' => 'true'
								)
							));

							return [
								'status' => 1,
								'pushtoken_id' => $pushtoken->id,
								'is_new' => false
							];
						}	
					}
				}

				/* Registrar el push token en Amazon SNS */
				try{
					$registration = $SNS->createPlatformEndpoint(array(
						'PlatformApplicationArn' => $applicationARN,
						'Token' => $token
					));

					$endpointARN = $registration->get('EndpointArn');

					// Forzar que el endpoint este habilitado en caso de que se haya deshabilitado previamente (ej. al desinstalar el app o desactivar las noticiaciones a nivel del OS)
					$SNS->setEndpointAttributes(array(
						'EndpointArn' => $endpointARN,
						'Attributes' => array(
							'Enabled' => 'true'
						)
					));
				}
				catch(Exception $e){
					$this->error(_T("No se pudo registrar el dispositivo en SNS (Error: %s)", $e->getMessage()));
				}

				if(!$registration) $this->error(_T("No se pudo leer la respuesta de registro en SNS"));

				/* Subscribir el push token (SNS Endpoint) al SNS Topic */
				try{
					$endpointARN = $registration->get('EndpointArn');

					$subscription = $SNS->subscribe(array(
						'TopicArn' => $topicARN,
						'Protocol' => 'application',
						'Endpoint' => $endpointARN
					));
					
					if($subscription){
						$subscriptionARN = $subscription->get('SubscriptionArn');
					}
				}
				catch(Exception $e){
					$this->error(_T("No se pudo realizar la suscripcion en SNS (%s)", $e->getMessage()));
				}
			}

			/* Crear el push token en la base de datos */
			try{
				$pushtoken = pushtoken::create($token, $platform, $endpointARN);
			}
			catch(Exception $e){
				$errorMessage = $e->getMessage();

				// No mostrar errores de tokens duplicados ya que estos estan siendo causados por race conditions derivados de multiples llamadas secuenciales del evento "registered" del phonegap-push-plugin a pesar de que tenemos logica para evitar inserciones duplicadas.
				if(preg_match("/Duplicate entry.+for key 'token'/i", $errorMessage)){
					return [
						'status' => 1,
						'pushtoken_id' => 0,
						'is_new' => false
					];
				}

				$this->error(_T('No se pudo registrar el push token en la base de datos (Error: %s).', $errorMessage));
			}

			/* Registrar la suscripcion al SNS Topic en la base de datos */
			$pushtoken->amazonsns_subscription_arn = $subscriptionARN;
			$pushtoken->save();
			
			/*
			try{
				$pushtoken->subscribe($topic->id, $subscriptionARN, false);
			}
			catch(Exception $e){
				$this->error(_T('No se pudo suscribir el push token (Error: %s).', $e->getMessage()));
			}
			*/

			/* 7. Responder */
			return [
				'status' => 1,
				'pushtoken_id' => $pushtoken->id,
				'is_new' => true
			];
		}
		else{
			$this->error(_T("Push token no proporcionado."));
		}
	}
}