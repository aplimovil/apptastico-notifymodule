<?php
class LocalNotify_ApiNotifyUpdateTokenController extends APIController {
	function main($APP, $GET, $POST){
		$token = trim($_REQUEST['token']);
		$action = trim($_REQUEST['action']);

		if($token != ''){
			$SNS = Notify\System::getSNSInstance();

			$pushtoken = pushtoken::findOne(['token' => $token]);

			if(!$pushtoken){
				$this->error(_T('El pushtoken no existe.'));
			}

			if($action == 'subscribe'){
				$this->subscribe($pushtoken);
			}
			elseif($action == 'unsubscribe'){
				$this->unsubscribe($pushtoken);
			}
			else{
				$this->error(_T('Accion invalida.'));
			}
		}
		else{
			$this->error(_T('Push token no proporcionado.'));
		}
	}

	function subscribe($pushtoken){
		$topicARN = trim(Notify\Config::getOption('aws_sns_topic_arn_list'));
		/*
		try{
			$topic = Notify\System::getTopicFromList();
			$topicARN = $topic->arn;
		}
		catch(Exception $e){
			$this->error(_T('No se encontró ningún Topic ARN disponible para realizar la suscripción (Error: %s).', $e->getMessage()));
		}*/

		try{
			// Forzar que el endpoint este habilitado en caso de que se haya deshabilitado previamente (ej. al desinstalar el app o desactivar las noticiaciones a nivel del OS)
			$SNS->setEndpointAttributes(array(
				'EndpointArn' => $pushtoken->amazonsns_endpoint_arn,
				'Attributes' => array(
					'Enabled' => 'true'
				)
			));

			if($pushtoken->amazonsns_subscription_arn == ''){
				$subscription = $SNS->subscribe(array(
					'TopicArn' => $topicARN,
					'Protocol' => 'application',
					'Endpoint' => $pushtoken->amazonsns_endpoint_arn
				));

				$subscriptionARN = $subscription->get('SubscriptionArn');

				$pushtoken->subscribe($topic->id, $subscriptionARN);

				$this->respond(array(
					'status' => 1
				));
			}
			else throw new Exception(_T('El suscriptor ya esta suscrito'));
		}
		catch(Exception $e){
			$this->error('No se pudo realizar la suscripcion en SNS (Error: %s)', $e->getMessage());
		}
	}

	function unsubscribe($pushtoken){
		if($pushtoken->amazonsns_subscription_arn != ''){
			try{
				$result = $SNS->unsubscribe(array(
					'SubscriptionArn' => $pushtoken->amazonsns_subscription_arn
				));

				$pushtoken->unsubscribe();

				$this->respond(array(
					'status' => 1
				));
			}
			catch(Exception $e){
				$this->error(_T('No se pudo realizar la suscripcion en SNS (Error: %s)', $e->getMessage()));
			}
		}
		else{
			$this->error(_T('Su dispositivo ya esta desuscrito.'));
		}
	}
}