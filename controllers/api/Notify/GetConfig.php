<?php
use Notify\Config as Config;

class LocalNotify_ApiNotifyGetConfigController extends APIController {
	function main($APP, $GET, $POST){
		$config = [
			'sender_name' => Config::getOption('sender_name'),
			'android_sender_id' => Config::getOption('android_sender_id'),
			'use_confirmations' => Config::$useConfirmations
		];

		return [
			'status' => 1,
			'result' => $config
		];
	}
}