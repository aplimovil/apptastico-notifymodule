<?php
class LocalNotify_ApiNotifyConfirmNotificationController extends APIController {
	function main($APP, $GET, $POST){
		$this->authorize();
		
		if(!is_numeric($POST->notification_id) || ltrim($POST->person_model) == '' || ltrim($POST->person_entity_id) == ''){
			$this->error(_T('Parámetros requeridos no proporcionados.'));
		}

		$notification = notification::get($POST->notification_id);
		if(!$notification) $this->error(_T('Notificación no encontrada.'));

		DB::Begin();

		$params = [
			'notification_id' => $notification->id,
			'person_model' => $POST->person_model,
			'person_entity_id' => $POST->person_entity_id
		];

		if(DB::Count('notification_confirmation', $params) == 0){
			$notificationConfirmation = (new notification_confirmation($params))->set([
				'datetime' => date('Y-m-d H:i:s')
			]);

			$notificationConfirmation->save();

			$notification->confirmation_count ++;
			$notification->save();

			DB::Commit();

			$this->send();
		}
		else{
			$this->send(['message' => 'Confirmación ya registrada.']);
		}
	}
}