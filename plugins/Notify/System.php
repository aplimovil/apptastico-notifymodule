<?php
namespace Notify;

use \AutoLoader;
use \amazonsns_topic;
use \Exception;
use \Aws;
use \Guzzle;

class System {
	public static function getSNSInstance(){
		global $APP;

		$key = Config::getOption('aws_credentials_key');
		$secret = Config::getOption('aws_credentials_secret');
		$region = Config::getOption('aws_region');

		if($key == '') throw new Exception(_T('Aun no se ha configurado el Access Key ID de AWS.'));
		if($secret == '') throw new Exception(_T('Aun no se ha configurado el Access Secret Key de AWS.'));
		if($region == '') throw new Exception(_T('Aun no se ha configurado el region ID de AWS.'));

		$SNS = Aws\Sns\SnsClient::factory([
		    'key' => $key,
		    'secret' => $secret,
		    'region'  => $region
		]);

		return $SNS;
	}

	public static function getSubscriberCount(){
		$count = \DB::Count('pushtoken', ['active' => 1]);
		return $count;
	}

	public static function getNotificationsSentThisMonth(){
		$monthStart = date('Y-m-d');
		$monthEnd = date('Y-m-t');
		$notificationsSentThisMonthResult = \DB::Query("SELECT SUM(recipient_count) AS count FROM notification WHERE status = " . NOTIFICATION_STATUS_SENT . " AND DATE(datetime_sent) BETWEEN '{$monthStart}' AND '{$monthEnd}'");
		$notificationsSentThisMonth = $notificationsSentThisMonthResult->count;

		return $notificationsSentThisMonth;
	}

	public static function getRemainingNotificationsThisMonth(){
		$monthlyLimit = Config::$LIMIT_MONTHLY_NOTIFICATIONS;
		$notificationsSentThisMonth = self::getNotificationsSentThisMonth();
		$remainingNotifications = $monthlyLimit - $notificationsSentThisMonth;
		return $remainingNotifications;
	}

	public static function sendPushNotification($message, $customData, $topicARNList = null, $endpointARNList = null){
		$SNS = System::getSNSInstance();

		$customDataJson = json_encode($customData);

		$notificationObject = [
			'MessageStructure' => 'json',
			'Message' => json_encode([
				'default' => $message,
				'APNS' => json_encode([
					'aps' => array(
						'alert' => $message,
						'sound' => 'default'
					),
					'custom' => $customDataJson
				]),
				'GCM' => json_encode([
					'data' => [
						'title' => Config::getOption('sender_name'),
						'message' => $message,
						'msgcnt' => 1,
						notification::$customDataFieldName => $customDataJson
					]
				])
			])
		];

		$messageIds = [];
		$requestIds = [];

		if($topicARNList !== false){
			if(is_null($topicARNList)){
				$topicARNList = explode("\n", trim(Config::getOption('aws_sns_topic_arn_list')));
			}
		}
		
		if($topicARNList){
			foreach($topicARNList as $topicARN){
				$topicARN = trim($topicARN);

				if($topicARN != ''){
					$notificationObject['TopicArn'] = $topicARN;

					try{
						$result = $SNS->publish($notificationObject);
						$resultData = (object) $result->getAll();

						$messageIds[] = $resultData->MessageId;
						$requestIds[] = $resultData->ResponseMetadata['RequestId'];
					}
					catch(Exception $e){
						throw new Exception(_T('Ocurrio un error al intentar enviar la notificación a los subscriptores del Amazon SNS Topic ARN "%s" (Error: %s)', $topicARN, $e->getMessage()));
					}
				}
			}

			
		}

		if($endpointARNList){
			if($notificationObject['TopicArn']){
				unset($notificationObject['TopicArn']);
			}

			foreach($endpointARNList as $endpointARN){
				$notificationObject['TargetArn'] = $endpointARN;

				try{
					$result = $SNS->publish($notificationObject);
					$resultData = (object) $result->getAll();

					$messageIds[] = $resultData->MessageId;
					$requestIds[] = $resultData->ResponseMetadata['RequestId'];
				}
				catch(Exception $e){
					$message = $e->getMessage();
					if(!preg_match('/endpoint.+disabled/i', $message)){ // Ignore "Endpoint is disabled" errors from Amazon SNS. It means the device's push token is no longer valid.
						throw new Exception(_T('Aviso: No se pudo enviar la notificación al suscriptor "%s" de Amazon SNS (Error: %s)', $endpointARN, $message));
					}
				}
			}
		}

		return (object) [
			'messageIds' => $messageIds,
			'requestIds' => $requestIds
		];
	}

	/*
	// Habilitar cuando se necesite soportar mas de 10 millones de suscriptores.
	// TODO: Validar topic ARN en Amazon SNS antes de agregarlo a la tabla "amazonsns_topic".

	public static function getTopicFromList(){
		$topicARNList = trim(Config::getOption('aws_sns_topic_arn_list'));

		// Popular tabla local de Amazon SNS Topics para llevar conteo de suscriptores
		if($topicARNList != ""){
			$topicARNList = explode("\n", $topicARNList);

			foreach($topicARNList as $topicARN){
				$topicARN = trim($topicARN);

				if(!($topic = amazonsns_topic::findOne(['arn' => $topicARN]))){
					$topic = new amazonsns_topic([
						'arn' => $topicARN,
						'subscriber_count' => 0
					]);

					$topic->save();
				}
			}
		}
		else throw new Exception(_T('Aun no esta configurada la lista de Topic ARNs de Amazon SNS.'));

		// Seleccionar el Topic con menos suscriptores y que aun tenga espacio disponible
		if($topic = amazonsns_topic::findOne(['subscriber_count' => ['<', AMAZON_SNS_LIMIT_SUBSCRIBERS_PER_TOPIC]], 'subscriber_count ASC', 1)){
			return $topic;
		}
		else{
			throw new Exception(_T('No se encontró ningun SNS Topic disponible.'));
		}
	}
	*/
}