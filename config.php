<?php
namespace Notify;

use APP, Lang;

const NOTIFICATION_STATUS_PENDING = 1;
const NOTIFICATION_STATUS_SENT = 2;

// Limite de 10 millones de suscriptores por Amazon SNS Topic: http://aws.amazon.com/sns/faqs/#limits-restrictions
const AMAZON_SNS_LIMIT_SUBSCRIBERS_PER_TOPIC = 10000000;

class Config extends \System\Config {
	static $moduleName = 'Notify';

	static $defaultOptions = [
		'aws_region' => 'us-east-1'
	];

	static $showMenu = true;
	static $showNotificationsMenuItem = true;
	static $allowCreateNotifications = true;
	static $useReadTracking = true;
	static $useReadReport = true;
	static $useConfirmations = false;
	static $usePersonTracking = false;
	static $encodeCustomData = false; // DEPRECATED

	static $NOTIFICATION_STATUS;

	static $UPLOAD_FOLDER_IMAGES = 'notify/notifications/images';

	// Limitar uso a 1 millon de push notifications mensuales por default
	static $LIMIT_MONTHLY_NOTIFICATIONS = 1000000;
}

APP::routes([
	'admin/notify/notification/{id}/reads' => '?notificationId=$1',
	'admin/notify/notification/{id}/confirmations' => '?notificationId=$1'
]);

APP::onConfigLoad(function(){
	global $APP, $ADMIN_MENU;

	$APP->loadModule('local/AWS', false);

	Lang::load('Notify');

	Config::$NOTIFICATION_STATUS = [
		NOTIFICATION_STATUS_PENDING => _T('Pendiente'),
		NOTIFICATION_STATUS_SENT => _T('Enviada')
	];

	if(Config::$showMenu){
		APP::extend($ADMIN_MENU, [
			['path' => 'admin/notify', 'label' => _T('Notificaciones'), 'inactive' => true, 'items' => [
				(Config::$showNotificationsMenuItem ? ['path' => 'notifications', 'label' => _T('Notificaciones')] : null),
				['path' => 'config', 'label' => _T('Configuración'), 'attributes' => ['class' => 'master-admin']],
			]]
		]);
	}

	APP::extend(Config::$defaultOptions, [
		'sender_name' => $APP->getName()
	]);
});