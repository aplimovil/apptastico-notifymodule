<script type="text/javascript">
var Notify = {
	Notifications: {
		sendNotification: function(href, showUserCount){
			var notificationId = parseInt(href.split("#")[1], 10);

			if(notificationId){
				var message = "<?= _T('¿Esta seguro que desea enviar esta notificación?') ?>";

				if(showUserCount){
					message = "<?= _T('¿Esta seguro que desea enviar esta notificación a los %s usuarios suscritos?', number_format($this->totalRecipients)) ?>";
				}

				if(window.confirm(message)){
					window.location = '?cmd=send-notification&id=' + notificationId + "&token=<?= $APP->getGlobalToken() ?>";
				}
			}
		}
	}
};
</script>

<p><strong><?= _T('Dispositivos Registrados') ?></strong>: <?= number_format($this->totalRecipients) ?></p>

<? $this->view() ?>