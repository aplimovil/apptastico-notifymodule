--
-- Table structure for table `amazonsns_topic`
--

CREATE TABLE `amazonsns_topic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `arn` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subscriber_count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `arn` (`arn`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `datetime_sent` datetime DEFAULT NULL,
  `recipient_count` int(10) unsigned DEFAULT NULL,
  `read_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `request_confirmations` tinyint(3) unsigned DEFAULT NULL,
  `confirmation_button_label` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation_count` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pushtoken`
--

CREATE TABLE `pushtoken` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `platform` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL,
  `amazonsns_topic_id` int(10) unsigned DEFAULT NULL,
  `amazonsns_endpoint_arn` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amazonsns_subscription_arn` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  KEY `plataform` (`platform`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- --------------------------------------------------------

--
-- Table structure for table `notification_read`
--

CREATE TABLE `notification_read` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `notification_id` int(10) unsigned NOT NULL,
  `person_model` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_entity_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `notification_id` (`notification_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `notification_confirmation`
--

CREATE TABLE `notification_confirmation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `notification_id` int(10) unsigned NOT NULL,
  `person_model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `person_entity_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `notification_id` (`notification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;