<?php
namespace Notify;

use Model, Validation;

class pushtoken extends Model {
	use Validation;

	public $id;
	public $token;
	public $platform;
	public $datetime;
	public $amazonsns_topic_id;
	public $amazonsns_endpoint_arn;
	public $amazonsns_subscription_arn;
	public $active;

	public static function create($token, $platform, $endpointARN, $datetime = null, $active = 1){
		if(!$datetime) $datetime = date('Y-m-d H:i:s');

		$pushtoken = static::findOne(['token' => $token]);

		if(!$pushtoken){
			$class = get_called_class();
			
			$pushtoken = (new $class)->set([
				'token' => $token,
				'platform' => $platform
			]);
		}

		$pushtoken->set([
			'datetime' => $datetime,
			'amazonsns_endpoint_arn' => $endpointARN,
			'active' => $active
		]);

		$pushtoken->save();

		return $pushtoken;
	}

	/*
	public function subscribe($topicId, $subscriptionARN, $errorIfAlreadySubscribed = true){
		$topic = amazonsns_topic::get($topicId);
		if(!$topic) throw new Exception(_T('No se encontro el Amazon SNS Topic ID #%s', $topicId));

		if($this->amazonsns_subscription_arn == ''){
			DB::Begin();

			$this->set([
				'amazonsns_topic_id' => $topic->id,
				'amazonsns_subscription_arn' => $subscriptionARN,
				'active' => 1
			]);

			if($this->save()){
				$topic->subscriber_count ++;

				if($topic->save()){
					DB::Commit();
					return true;
				}
				else{
					DB::Rollback();
					throw new Exception(_T('No se pudo actualizar el Amazon SNS Topic en la base de datos.'));
				}
			}
			else{
				DB::Rollback();
				throw new Exception(_T('No se pudo actualizar el push token en la base de datos.'));
			}
		}
		else{
			if($errorIfAlreadySubscribed){
				throw new Exception(_T('El suscriptor ya esta suscrito.'));
			}
			else{
				return true;
			}
		}
	}

	public function unsubscribe($topicId, $subscriptionARN){
		$topic = amazonsns_topic::get($topicId);
		if(!$topic) throw new Exception(_T('No se encontro el Amazon SNS Topic ID #%s', $topicId));

		if($this->amazonsns_subscription_arn != ''){
			DB::Begin();

			$this->set([
				'amazonsns_subscription_arn' => '',
				'active' => 0
			]);

			if($this->save()){
				$topic->subscriber_count --;

				if($topic->save()){
					DB::Commit();
					return true;
				}
				else{
					DB::Rollback();
					throw new Exception(_T('No se pudo actualizar el Amazon SNS Topic en la base de datos.'));
				}
			}
			else{
				DB::Rollback();
				throw new Exception(_T('No se pudo actualizar el push token en la base de datos.'));
			}
		}
		else throw new Exception(_T('El suscriptor ya esta desuscrito.'));
	}
	*/
}