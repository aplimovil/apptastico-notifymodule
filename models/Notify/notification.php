<?php
namespace Notify;

use APIFilters, Model, Entity, SoftDeletes, Validation, Uploads;

class notification extends Model implements APIFilters {
	use Entity, SoftDeletes, Validation, Uploads;
	
	/* Config */
	static $table = 'notification';
	static $route = 'admin/notify/notifications';
	static $filterRoute = false;
	static $api = [
		'gzip' => true,
		'filter' => [
			'status' => NOTIFICATION_STATUS_SENT
		]
	];
	static $customDataFieldName = 'custom';

	/* Fields */
	public $id;
	public $subject;
	public $image;
	public $content;
	public $datetime_sent;
	public $recipient_count;
	public $read_count;
	public $data;
	public $request_confirmations;
	public $confirmation_button_label;
	public $confirmation_count;
	public $status;

	/* Properties */
	public $excerpt;
	public $date;

	public static function API_Before(&$params){}
	
	public static function API_Query($params, &$columns, &$from, &$where, &$having, &$order, &$limit){}

	public static function API_Item(&$notification, $params){
		$image = $notification->getUpload('image');
		$notification->image = $image ? $image->abswebpath : '';
		$notification->excerpt = $notification->getExcerpt();
		$notification->date = $notification->getDate();
	}

	public static function API_Results(&$results, $params){}

	function getExcerpt($length = 50){
		global $APP;

		$plainContent = strip_tags(html_entity_decode($this->content, ENT_COMPAT, $APP->getCharset()));
		$excerpt = mb_substr($plainContent, 0, $length, $APP->getCharset());
		if(strlen($excerpt) < strlen($plainContent)) $excerpt .= '...';
		return $excerpt;
	}

	function getDate(){
		return $this->datetime_sent ? date('d/m/Y', strtotime($this->datetime_sent)) : '';
	}

	function getPushCustomData(){
		return [
			'type' => 'notification',
			'id' => $this->id,
			'data' => $this->data
		];
	}
	
	function send(){
		global $APP;

		$SNS = System::getSNSInstance();

		$customData = json_encode($this->getPushCustomData());

		if(Config::$encodeCustomData){ // DEPRECATED
			$customData = rawurlencode($customData);
		}

		$notificationObject = [
			'MessageStructure' => 'json',
			'Message' => json_encode([
				'default' => $this->subject,
                'APNS' => json_encode([
                    'aps' => array(
                            'alert' => $this->subject,
                            'sound' => 'default',
                    ),  
                    self::$customDataFieldName => json_decode($customData)
                ]), 
                'APNS_SANDBOX' => json_encode([
                    'aps' => array(
                            'alert' => $this->subject,
                            'sound' => 'default',
                    ),  
                    self::$customDataFieldName => json_decode($customData)
                ]),
				'GCM' => json_encode([
					'priority' => 'high',
					'data' => [
						'title' => Config::getOption('sender_name'),
						'message' => $this->subject,
						'msgcnt' => 1,
						self::$customDataFieldName => $customData
					]
				])
			])
		];

		$topicARNList = trim(Config::getOption('aws_sns_topic_arn_list'));
		$messageIds = [];
		
		if($topicARNList != ''){
			$topicARNList = explode("\n", $topicARNList);

			foreach($topicARNList as $topicARN){
				$topicARN = trim($topicARN);

				if($topicARN != ''){
					$notificationObject['TopicArn'] = $topicARN;

					try{
						$result = $SNS->publish($notificationObject);
						$resultData = (object) $result->getAll();
					}
					catch(Exception $e){
						$message = $e->getMessage();
						throw new Exception(_T('Ocurrio un error al intentar enviar la notificación a los subscriptores del Amazon SNS Topic ARN "%s" (Error: %s)', $topicARN, $e->getMessage()));
					}

					$messageIds[] = $resultData->MessageId;
				}
			}

			if(count($messageIds) != 0){
				$this->datetime_sent = date('Y-m-d H:i:s');
				$this->recipient_count = System::getSubscriberCount();
				$this->status = NOTIFICATION_STATUS_SENT;
				$this->save();

				return (object) [
					'messageIds' => $messageIds
				];
			} else {
				throw new Exception(_T('La notificación no se envío a ningún destinatario.'));
			}
		} else{
			throw new Exception(_T('Aun no esta configurada la lista de Topic ARNs de Amazon SNS.'));
		}
	}
}