<?php
class notification_confirmation extends Model {
	use Entity, Validation;

	static $route = 'admin/notify/notification/confirmations';
	
	public $id;
	public $notification_id;
	public $person_model;
	public $person_entity_id;
	public $datetime;
}