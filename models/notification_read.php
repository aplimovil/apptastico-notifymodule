<?php
class notification_read extends Model {
	use Validation;

	public $id;
	public $notification_id;
	public $person_model;
	public $person_entity_id;
	public $ip_address;
	public $datetime;
}